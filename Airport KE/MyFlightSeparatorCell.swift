//
//  MyFlightSeparatorCell.swift
//  Airport KE
//
//  Created by Martin Krasnocka on 03/01/2019.
//  Copyright © 2019 com.krlg. All rights reserved.
//

import UIKit

class MyFlightSeparatorCell: UITableViewCell {

    @IBOutlet weak var containerView: UIView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        applyContainerStyle()
    }
    
    func applyContainerStyle() {
        containerView.layer.cornerRadius = 0
        containerView.layer.shadowColor = UIColor.darkGray.cgColor
        containerView.layer.shadowRadius = 10
        containerView.layer.shadowOffset = CGSize(width: 12, height: 8)
        containerView.layer.shadowOpacity = 0.9
        containerView.layer.masksToBounds = false
    }
}

class TopRadius: UIView
{
    let radius: CGFloat = 15
    
    override func draw(_ rect: CGRect) {
        super.draw(rect)
    
        let path = UIBezierPath()
        path.addArc(withCenter: CGPoint(x: rect.origin.x + radius, y: rect.origin.y + rect.size.height), radius: radius, startAngle: 0, endAngle: -CGFloat(Double.pi/2), clockwise: true)
        path.addLine(to: CGPoint(x: rect.origin.x + rect.size.width - radius, y: path.currentPoint.y))
        path.addArc(withCenter: CGPoint(x: path.currentPoint.x, y: path.currentPoint.y + radius), radius: radius, startAngle: -CGFloat(Double.pi/2), endAngle: CGFloat(Double.pi), clockwise: true)
        UIColor.white.setFill()
        path.close()
        path.fill()
    }

}
