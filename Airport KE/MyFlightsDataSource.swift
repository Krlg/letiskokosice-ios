//
//  MyFlightsDataSource.swift
//  Airport KE
//
//  Created by Karol Grulling on 17/04/2018.
//  Copyright © 2018 com.krlg. All rights reserved.
//

import UIKit

protocol FlightRemovedDelegate {
    func flightRemoved()
}

class MyFlightsDataSource: NSObject, UITableViewDataSource {
    
    var flights = [Flight]()
    let MyFlightCellIdentifier = "MyFlightCell"
    var flightRemovedDelegate: FlightRemovedDelegate?
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return flights.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: MyFlightCellIdentifier, for: indexPath) as! MyFlightCell
        cell.initWithFlight(flight: flights[indexPath.row])
        return cell
    }
    
    func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCell.EditingStyle, forRowAt indexPath: IndexPath) {
        if editingStyle == .delete {
            let selectedFlight = flights[indexPath.row]
            CoreDataManager.sharedManager.deleteStoredFlight(flightNumber: selectedFlight.flightNumber!)
            flights.remove(at: indexPath.row)
            tableView.deleteRows(at: [indexPath], with: UITableView.RowAnimation.fade)
            flightRemovedDelegate?.flightRemoved()
        }
    }
}
