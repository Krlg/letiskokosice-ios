//
//  RewardsRequester.swift
//  Airport KE
//
//  Created by Ales Melichar on 08/11/2019.
//  Copyright © 2019 com.krlg. All rights reserved.
//

import Foundation
import Alamofire

class RewardsRequester: CoreRequester{
    static let sharedRequester = RewardsRequester()

    func getRewards(completionHandler: @escaping (_ success: Bool, _ rewards: [Reward]?) -> Void){
        let url = Constants.URLConstants.RewardsUrl
        print("Toto je url: ", url)
        AF.request(url, method: .get, parameters: nil, encoding: JSONEncoding.default, headers: nil).responseJSON(completionHandler: { response in
            print("Response REWARDS String: \(response.result)")

            guard response.response?.statusCode == 200 else {
                completionHandler(false, nil)
                return
            }
            
            switch response.result {
            case let .success(value):
                var rewards = [Reward]()
                let rewardsResult = value as! [String:Any]
                let rewardsData = rewardsResult["rewards"] as! [[String:Any]]
                do {
                    print("RewardsData: ", rewardsData)
                    for rewardJson in rewardsData {
                        print("Som v reward cykle!")
                        let reward = try Reward.init(json: rewardJson)
                        rewards.append(reward)
                    }
                    print(rewards)
                }catch let error as NSError {
                    print("Vyskytol sa error! ", error.description)
                }
                completionHandler(true, rewards)
            case .failure(_):
                completionHandler(false, nil)
            }
        })
    }
}

