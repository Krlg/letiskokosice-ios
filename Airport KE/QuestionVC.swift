//
//  QuizVC.swift
//  Airport KE
//
//  Created by Karol Grulling on 24/07/2018.
//  Copyright © 2018 com.krlg. All rights reserved.
//

import UIKit

class QuestionVC: QuizBaseVC {
    
    @IBOutlet weak var questionIndexLabel: UILabel?
    @IBOutlet weak var questionTextLabel: UILabel?
    @IBOutlet var answerButtons: [UIButton]?
    @IBOutlet weak var nextQuestionBtn: UIButton?
    @IBOutlet weak var getResultsBtn: UIButton?
    
    var question: Question?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupWithQuestion()
    }
    
    func setupWithQuestion() {
        questionIndexLabel?.text = String(pageIndex+1) + "/" + String(QuizManager.sharedManager.getQuestionCount())
        questionTextLabel?.text = question?.text
        for button in answerButtons! {
            button.setTitle(question?.options[button.tag].name, for: UIControl.State.normal)
        }
        
        if (isLastQuestion()) {
            nextQuestionBtn?.isHidden = true
            getResultsBtn?.isHidden = false
        }
    }
    
    func isLastQuestion() -> Bool {
        return (pageIndex+1) == QuizManager.sharedManager.getQuestionCount()
    }
    
    @IBAction func didSelectAnswer(sender: UIButton) {
        sender.isSelected = true
        deselectButtons(selectedTag: sender.tag)
        question?.setAnswer(answer: (question?.options[sender.tag])!)
    }
    
    @IBAction func getResults() {
        QuizManager.sharedManager.moveToNextQuestion()
    }
    
    func deselectButtons(selectedTag: Int) {
        for button in answerButtons! {
            if button.tag != selectedTag {
                button.isSelected = false
            }
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    @IBAction func nextQuestionPressed() {
        QuizManager.sharedManager.moveToNextQuestion()
    }
}
