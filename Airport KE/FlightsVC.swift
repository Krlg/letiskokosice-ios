//
//  ViewController.swift
//  Airport KE
//
//  Created by Karol Grulling on 04/07/2017.
//  Copyright © 2017 com.krlg. All rights reserved.
//

import UIKit
import BetterSegmentedControl
//import DGRunkeeperSwitch

class FlightsVC: UIViewController, UITableViewDelegate {
    
    let switchBgColor = UIColor(red: 255/255.0, green: 174.0/255.0, blue: 18.0/255.0, alpha: 1.0)
    let selectedTitleColor = UIColor(red: 239.0/255.0, green: 95.0/255.0, blue: 49.0/255.0, alpha: 1.0)
    
    
    @IBOutlet weak var flightsTable: UITableView?
    let dataSource = FlightsDataSource()
    // @IBOutlet weak var flightsSwitch: DGRunkeeperSwitch?
    @IBOutlet weak var switchIconDeparture: UIImageView!
    @IBOutlet weak var switchIconArrival: UIImageView!
    @IBOutlet weak var arrivalsImage: UIImageView!
    @IBOutlet weak var departuresImage: UIImageView!
    @IBOutlet weak var switchControl: BetterSegmentedControl!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        flightsTable?.dataSource = dataSource
        flightsTable?.delegate = dataSource
        flightsTable?.rowHeight = UITableView.automaticDimension
        setupFlightSwitch()
        
        NotificationCenter.default.addObserver(self, selector: #selector(flightsDidChange(_:)), name: CoreDataManager.fligtsDidChangeNotificationName, object: nil)
    }
    func setupFlightSwitch(){
        
        dataSource.flightsType = FlightType.Departure
        arrivalsImage.tintColor = .black
        departuresImage.tintColor = .black
        
        switchControl.segments = LabelSegment.segments(withTitles: ["Arrivals", "Departures"],
                                                       normalFont: UIFont(name: "HelveticaNeue-Light", size: 14.0)!,
                                                       normalTextColor: .gray,
                                                       selectedTextColor: UIColor(red:1.00, green:0.70, blue:0.09, alpha:1.0))
    }
    
    //
    //    func setupFlightSwitch() {
    //        flightsSwitch?.titles = ["Departures", "Arrivals"]
    //        flightsSwitch?.backgroundColor = .white
    //        flightsSwitch?.selectedBackgroundColor = switchBgColor
    //        flightsSwitch?.titleColor = .gray
    //        flightsSwitch?.selectedTitleColor = .clear
    //        flightsSwitch?.titleFont = UIFont(name: "HelveticaNeue-Medium", size: 13.0)
    //        flightsSwitch?.autoresizingMask = [.flexibleWidth]
    //        flightsSwitch?.selectedBackgroundInset = 0
    //        flightsSwitch?.titleFontFamily = "DINPro"
    //        switchIconDeparture?.alpha = 1
    //        switchIconDeparture.image = UIImage(named: "departures")?.withRenderingMode(.alwaysTemplate)
    //        switchIconArrival.image = UIImage(named: "arrivals")?.withRenderingMode(.alwaysTemplate)
    //        switchIconDeparture.tintColor = .black
    //        switchIconArrival.tintColor = .black
    //    }
    
    //    @IBAction func switchFlights() {
    //        if flightsSwitch?.selectedIndex == 0 {
    //            dataSource.flightsType = FlightType.Departure
    //            switchIconDeparture?.alpha = 1
    //            switchIconArrival?.alpha = 0
    //        } else {
    //            dataSource.flightsType = FlightType.Arrival
    //            switchIconDeparture?.alpha = 0
    //            switchIconArrival?.alpha = 1
    //        }
    //        dataSource.expandedRows.removeAll()
    //        flightsTable?.reloadData()
    //    }
    
    @IBAction func saveFlight(sender: UIButton) {
        if dataSource.flightsType == FlightType.Arrival{
            CoreDataManager.sharedManager.saveFlight(flight: FlightHolder.sharedHolder.arrivals[sender.tag])
        } else {
            CoreDataManager.sharedManager.saveFlight(flight: FlightHolder.sharedHolder.departures[sender.tag])
        }
    }
    
    @IBAction func switchValueChanged(_ sender: BetterSegmentedControl) {
        if sender.index == 0 {
            arrivalsImage.isHidden = true
            departuresImage.isHidden = false
            dataSource.flightsType = FlightType.Departure
        }
        else {
            arrivalsImage.isHidden = false
            departuresImage.isHidden = true
            dataSource.flightsType = FlightType.Arrival
        }
        dataSource.expandedRows.removeAll()
        flightsTable?.reloadData()
    }
    
    @objc func flightsDidChange(_ sender: Notification?) {
        flightsTable?.reloadData()
    }
    
    deinit {
        NotificationCenter.default.removeObserver(self)
    }
    
}

