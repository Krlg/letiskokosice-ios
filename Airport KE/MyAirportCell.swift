//
//  MyAirportCell.swift
//  Airport KE
//
//  Created by Karol Grulling on 24/04/2018.
//  Copyright © 2018 com.krlg. All rights reserved.
//

import UIKit

class MyAirportCell: UICollectionViewCell {
    
    @IBOutlet weak var actionName: UILabel?
    @IBOutlet weak var actionIcon: UIImageView?
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }

    func initWithMyAirportAction(myAirportAction: MyAirportAction) {
        actionName?.text = myAirportAction.name
        actionIcon?.image = myAirportAction.icon
        self.layer.borderWidth = 2
        self.layer.borderColor = UIColor.lightGray.cgColor
    }
}
