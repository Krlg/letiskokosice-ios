//
//  ScoreManager.swift
//  Airport KE
//
//  Created by Karol Grulling on 01/08/2018.
//  Copyright © 2018 com.krlg. All rights reserved.
//

import UIKit

class ScoreManager: NSObject {
    
    static let QuizBonus = 100
    static let CheckInBonus = 50
    static let PhotoBonus = 50
    
    static let sharedManager = ScoreManager()
    let UserDefaultsScoreKey = "DefaultsScore"
    
    func getStoredScore() -> Int {
        return UserDefaults.standard.integer(forKey: UserDefaultsScoreKey)
    }
    
    func increaseScore(scoreToAdd: Int) {
        UserDefaults.standard.set(getStoredScore()+scoreToAdd, forKey: UserDefaultsScoreKey)
    }
    
    func decreaseScore(scoreToSubtract: Int) {
        UserDefaults.standard.set(getStoredScore()-scoreToSubtract, forKey: UserDefaultsScoreKey)
    }

}
