//
//  SplashVC.swift
//  Airport KE
//
//  Created by Karol Grulling on 18/07/2017.
//  Copyright © 2017 com.krlg. All rights reserved.
//

import UIKit

class SplashVC: UIViewController {
    
    @IBOutlet weak var planeImage: UIImageView?
    @IBOutlet weak var planeConstraint: NSLayoutConstraint?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        let quizesJson = Utils.loadJsonFromFile(fileName: "airport_quizes")
        do {
            for quizJson in quizesJson!["quizes"] as! [[String:Any]]{
                CoreDataManager.sharedManager.saveQuiz(quiz: try Quiz.init(json: quizJson))
            }
        } catch let error as NSError {
            print("Parsing quizes failed with error %@", error.description)
        }
        
        RewardsRequester.sharedRequester.getRewards(completionHandler: {success, rewards in
            if success == true && (rewards != nil){
                for i in rewards! {
                    print("TOTOK JE I REWARDS", i)
                }
                RewardHolder.sharedHolder.rewards = rewards!
                print("Rewards were successfuly appended.")
            }
        })
        
        FlightsRequester.sharedRequester.getFlights(completionHandler: { success, arrivals, departures in
            if success == true && (arrivals != nil && departures != nil) {
                FlightHolder.sharedHolder.arrivals = arrivals!
                FlightHolder.sharedHolder.departures = departures!
                self.animatePlane()
            } else {
                self.animatePlane()
            }
        })
    }

    func enterApp() {
        self.present((self.storyboard?.instantiateViewController(withIdentifier: Constants.StoryboardConstants.MainTabBar))!, animated: true, completion: nil)
    }
    
    func animatePlane() {
        UIView.animate(withDuration: 3, animations: {() -> Void in
            self.planeConstraint?.constant = -500
            self.view.layoutIfNeeded()
        }, completion:{ success in
            self.enterApp()
        })
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
}
