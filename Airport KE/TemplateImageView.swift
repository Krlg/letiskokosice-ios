//
//  TemplateImageView.swift
//  Airport KE
//
//  Created by Martin Krasnocka on 02/01/2019.
//  Copyright © 2019 com.krlg. All rights reserved.
//

import UIKit

class TemplateImageView: UIImageView {

    override func awakeFromNib() {
        super.awakeFromNib()
        let color = tintColor
        tintColor = nil
        tintColor = color
    }

}
