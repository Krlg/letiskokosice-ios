//
//  AnimationUtils.swift
//  Airport KE
//
//  Created by Karol Grulling on 17/07/2018.
//  Copyright © 2018 com.krlg. All rights reserved.
//

import UIKit

class AnimationUtils: NSObject {
    static func fadeInAndOutView(view: UIView) {
        UIView.animate(withDuration: 2, animations: {
            view.alpha = 1.0
        }, completion: { success in
            fadeOutView(view: view)
        })
    }
    
    static func fadeInView(view: UIView) {
        UIView.animate(withDuration: 1, animations: {
            view.alpha = 1.0
        })
    }
    
    static func fadeOutView(view: UIView) {
        UIView.animate(withDuration: 1, animations: {
            view.alpha = 0
        })
    }
}
