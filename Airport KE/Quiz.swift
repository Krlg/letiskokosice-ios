//
//  Quiz.swift
//  Airport KE
//
//  Created by Karol Grulling on 24/04/2018.
//  Copyright © 2018 com.krlg. All rights reserved.
//

import UIKit
import CoreData

class Quiz: NSObject {
    var quizId: String?
    var title: String?
    var reward: Int
    var questions = [Question]()
    
    init(json: [String: Any]) throws {
        guard let jsonQuizId = json["id"] as? String else {
            throw SerializationError.missing("id")
        }
        guard let jsonTitle = json["title"] as? String else {
            throw SerializationError.missing("title")
        }
        
        guard let jsonReward = json["reward"] as? Int else {
            throw SerializationError.missing("reward")
        }
        
        guard let jsonQuestions = json["questions"] as? [[String:Any]] else {
            throw SerializationError.missing("questions")
        }
        
        self.quizId = jsonQuizId
        self.title = jsonTitle
        self.reward = jsonReward
        
        do {
            for jsonQuestion in jsonQuestions {
                questions.append(try Question.init(json: jsonQuestion))
            }
        } catch let error as NSError {
            print("Couldn't parse questions for quiz \(String(describing: self.title)). Error: \(error.description)")
        }
    }
    
    init(entity: NSManagedObject) {
        self.quizId = entity.value(forKey: Constants.CoreDataConstants.QuizConstants.EntityKeyQuizId) as? String
        self.title = entity.value(forKey: Constants.CoreDataConstants.QuizConstants.EntityKeyQuizTitle) as? String
        self.reward = (entity.value(forKey: Constants.CoreDataConstants.QuizConstants.EntityKeyReward) as? Int)!
        let questions = entity.value(forKey: Constants.CoreDataConstants.QuizConstants.EntityKeyQuestions) as! NSSet
        let questionsArray = questions.allObjects
        for questionEntity in questionsArray {
            self.questions.append(Question.init(entity: questionEntity as! NSManagedObject))
        }
    }
}
