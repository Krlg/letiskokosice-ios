//
//  FlightHolder.swift
//  Airport KE
//
//  Created by Karol Grulling on 18/07/2017.
//  Copyright © 2017 com.krlg. All rights reserved.
//

import UIKit

class FlightHolder: NSObject {
    static let sharedHolder = FlightHolder()
    var arrivals = [Flight]()
    var departures = [Flight]()
}
