//
//  MyAirportHeaderCollectionReusableView.swift
//  Airport KE
//
//  Created by Karol Grulling on 11/07/2018.
//  Copyright © 2018 com.krlg. All rights reserved.
//

import UIKit
import BetterSegmentedControl
//import DGRunkeeperSwitch

class MyAirportHeader: UICollectionReusableView {
    
 //   @IBOutlet weak var myAirportActionsSwitch: DGRunkeeperSwitch?
    @IBOutlet weak var scoreLabel: UILabel?
    
    @IBOutlet weak var switchControls: BetterSegmentedControl!
    
    override func awakeFromNib() {
        setupFlightSwitch()
//        myAirportActionsSwitch?.titles = ["ZÍSKAŤ", "VYMENIŤ"]
//        myAirportActionsSwitch?.backgroundColor = UIColor.lightGray
//        myAirportActionsSwitch?.selectedBackgroundColor = UIColor.darkGray
//        myAirportActionsSwitch?.titleColor = UIColor.darkGray
//        myAirportActionsSwitch?.selectedTitleColor = UIColor.white
//        myAirportActionsSwitch?.titleFontFamily = "DINPro"
//        myAirportActionsSwitch?.autoresizingMask = [.flexibleWidth]
        
    }
    func setupFlightSwitch(){
        switchControls?.segments = LabelSegment.segments(withTitles: ["Získať", "Vymeniť"],
                                                        normalFont: UIFont(name: "HelveticaNeue-Light", size: 14.0)!,
                                                        normalTextColor: .gray,
                                                        selectedTextColor: UIColor(red:1.00, green:0.70, blue:0.09, alpha:1.0))
    }
    @IBAction func switchAction(_ sender: Any) {
        
    }
}
