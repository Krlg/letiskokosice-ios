//
//  Flight.swift
//  Airport KE
//
//  Created by Karol Grulling on 04/07/2017.
//  Copyright © 2017 com.krlg. All rights reserved.
//

import UIKit
import CoreData

enum SerializationError: Error {
    case missing(String)
}

enum FlightType: Int {
    case Departure
    case Arrival
}

class Flight: NSObject {
    var airline: String?
    var estimatedTime: Date?
    var flightNumber: String?
    var city: String?
    var scheduledTime: Date?
    var status: String?
    var gate: String?
    var flightType: FlightType?
    
    init(json: [String: Any], flightType: FlightType) throws {
        guard let jsonAirline = json["airline"] as? String else {
            throw SerializationError.missing("airline")
        }
        guard let jsonEstimatedTime = json["estimated"] as? String else {
            throw SerializationError.missing("estimated")
        }
        
        guard let jsonFlightNumber = json["flightNumber"] as? String else {
            throw SerializationError.missing("flightNumber")
        }
    
        guard let jsonStatus = json["status"] as? String else {
            throw SerializationError.missing("status")
        }
        
        guard let jsonScheduledTime = json["scheduled"] as? String else {
            throw SerializationError.missing("scheduled")
        }
    
        if flightType == FlightType.Departure {
            guard let jsonTo = json["to"] as? String else {
                throw SerializationError.missing("to")
            }
            self.city = jsonTo
        } else {
            guard let jsonFrom = json["from"] as? String else {
                throw SerializationError.missing("from")
            }
            self.city = jsonFrom
        }
        
        self.flightType = flightType
        self.airline = jsonAirline
        self.estimatedTime = Utils.dateFromString(string: jsonEstimatedTime)
        self.flightNumber = jsonFlightNumber
        self.scheduledTime = Utils.dateFromString(string: jsonScheduledTime)
        self.status = jsonStatus
        self.gate = json["gate"] as? String
    }
    
    init(entity: NSManagedObject) {
        self.city = entity.value(forKey: Constants.CoreDataConstants.FlightConstants.EntityKeyCity) as? String
        self.airline = entity.value(forKey: Constants.CoreDataConstants.FlightConstants.EntityKeyAirline) as? String
        self.scheduledTime = entity.value(forKey: Constants.CoreDataConstants.FlightConstants.EntityKeyScheduledTime) as? Date
        self.flightNumber = entity.value(forKey: Constants.CoreDataConstants.FlightConstants.EntityKeyFlightNumber) as? String
        self.flightType = FlightType.init(rawValue: entity.value(forKey: Constants.CoreDataConstants.FlightConstants.EntityKeyFlightType) as! Int)
    }
    
    func isOnTime() -> Bool? {
        if let estimatedTime = estimatedTime, let scheduledTime = scheduledTime {
            return estimatedTime.compare(scheduledTime) == ComparisonResult.orderedSame
        }
        return nil
    }
}
