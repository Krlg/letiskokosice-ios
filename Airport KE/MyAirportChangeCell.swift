//
//  MyAirportChangeCell.swift
//  Airport KE
//
//  Created by Ales Melichar on 31/07/2019.
//  Copyright © 2019 com.krlg. All rights reserved.
//

import UIKit

class MyAirportChangeCell: UICollectionViewCell {
    
    @IBOutlet weak var productThumbnail: UIImageView!
    @IBOutlet weak var productPrice: UILabel!
    @IBOutlet weak var productName: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
    func initCell(reward: Reward){
        let index = reward.name?.index((reward.name?.startIndex)!, offsetBy: 3)
        productName?.text = reward.name!.substring(to: index!)
        productPrice?.text = reward.price!.substring(to: index!)
        self.layer.borderWidth = 2
        self.layer.borderColor = UIColor.lightGray.cgColor
        
//        if let thumbnailPic = reward.thumbnail {
//            self.productThumbnail.setImage(with:  URL.init(string: reward.thumbnail),
//                                         placeholder: nil,
//                                         options: [.transition(.fade(1))],
//                                         progressBlock: nil,
//                                         completionHandler: nil)
//        }

    }
}

