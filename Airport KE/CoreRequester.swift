//
//  CoreRequester.swift
//  Airport KE
//
//  Created by Karol Grulling on 18/05/2017.
//  Copyright © 2017 com.krlg. All rights reserved.
//

import UIKit
import Alamofire

class CoreRequester: NSObject {
    
    func logResponse(response: DataResponse<Any>) {
        print(response.request as Any)
        print(response.response as Any)
        print(response.data as Any)
        print(response.result)
        
        switch response.result {
        case let .success(value):
            print("JSON: \(value)")
        case let .failure(error):
            break
        }
    }
    
    func printRequest(requestDict: [String:Any]) {
        do {
            let jsonData = try JSONSerialization.data(withJSONObject: requestDict, options: .prettyPrinted)
            let decoded = try JSONSerialization.jsonObject(with: jsonData, options: [])
            if let dictFromJSON = decoded as? [String:Any] {
                print(dictFromJSON)
            }
        } catch {
            print(error.localizedDescription)
        }
    }
}
