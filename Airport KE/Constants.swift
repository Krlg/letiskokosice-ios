//
//  Constants.swift
//  Airport KE
//
//  Created by Karol Grulling on 16/04/2018.
//  Copyright © 2018 com.krlg. All rights reserved.
//

import UIKit

struct Constants  {
    
    struct URLConstants {
        static let BaseUrl = "https://www.airportkosice.sk/api/"
        static let FlightsUrl = "arrivals_departures?_format=json&timestamp=12345"
        static let RewardsUrl = "https://www.airportkosice.sk/json/airport_rewards.json"
    }
    
    struct StoryboardConstants {
        static let StoryboardMain = "Main"
        static let MainTabBar = "MainTabBar"
        static let MyFlightsVC = "MyFlightsVC"
        static let QuizVC = "QuizVC"
        static let QuizPageVC = "QuizPageVC"
        static let QuestionVC = "QuestionVC"
        static let QuizResultVC = "QuizResultVC"
    }
    
    struct CoreDataConstants {
        struct FlightConstants {
            static let EntityNameMyFlight = "EntityMyFlight"
            static let EntityKeyAirline = "airline"
            static let EntityKeyFlightNumber = "flightNumber"
            static let EntityKeyScheduledTime = "scheduledTime"
            static let EntityKeyCity = "city"
            static let EntityKeyFlightType = "flightType"
        }
        
        struct QuizConstants {
            static let EntityNameQuiz = "EntityQuiz"
            static let EntityKeyQuizId = "quizId"
            static let EntityKeyQuizTitle = "title"
            static let EntityKeyReward = "reward"
            static let EntityKeyQuestions = "questions"
            static let EntityNameQuestion = "EntityQuestion"
            static let EntityKeyQuestionText = "text"
            static let EntityKeyQuestionOrder = "order"
            static let EntityKeyQuestionOptions = "options"
            static let EntityNameOption = "EntityOption"
            static let EntityKeyOptionName = "name"
            static let EntityKeyOptionIsCorrect = "isCorrect"
        }
        struct RewardConstants {
            static let EntityNameReward = "EntityReward"
            static let EntityKeyRewardId = "rewardId"
            static let EntityKeyRewardName = "name"
            static let EntityKeyRewardPrice = "price"
            static let EntityKeyThumbnail = "thumbnail"
        }
    }
}
