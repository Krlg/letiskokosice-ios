//
//  BeaconManager.swift
//  Airport KE
//
//  Created by Karol Grulling on 29/06/2018.
//  Copyright © 2018 com.krlg. All rights reserved.
//

import UIKit

protocol BeaconManagerDelegate {
    func beaconFound()
    func beaconNotFound()
    func notAuthorized()
    func bluetoothIsOn()
    func bluetoothFailureOccurred()
}

struct BeaconConstants {
    static let BeaconUuid = "B9407F30-F5F8-466E-AFF9-25556B57FE6D"
    static let BeaconMajor = 36127
    static let BeaconMinor = 11537
    static let BeaconIdentifier = "df6e64eeac57780cfe6d137e187a2834"
}

class BeaconManager: NSObject, ESTBeaconManagerDelegate, CBCentralManagerDelegate, CLLocationManagerDelegate {
    static let sharedManager = BeaconManager()
    let beaconManager = ESTBeaconManager()
    var delegate: BeaconManagerDelegate?
    var bluetoothManager:CBCentralManager!
    var locationManager = CLLocationManager()
    
    func setupWithDelegate(delegate: BeaconManagerDelegate) {
        self.delegate = delegate
        bluetoothManager = CBCentralManager()
        bluetoothManager.delegate = self
        locationManager.delegate = self
    }
    
    
    // MARK: Get location services permission
    
    private func validateLocationPermissions() {
        let authStatus = CLLocationManager.authorizationStatus()
        processLocationPermissionStatus(authStatus)
    }
    
    private func processLocationPermissionStatus(_ authStatus: CLAuthorizationStatus) {
        if authStatus == .notDetermined {
            // request 'always' permission
            locationManager.requestAlwaysAuthorization()
        } else if authStatus == .restricted || authStatus == .denied || authStatus == .authorizedWhenInUse {
            // permission denied or not sufficient
            delegate?.notAuthorized()
        } else if authStatus == .authorizedAlways {
            startBeaconMonitoring()
        }
    }
    
    
    // MARK: CBCentralManagerDelegate
    
    func centralManagerDidUpdateState(_ central: CBCentralManager) {
        switch central.state {
        case .poweredOn:
            self.delegate?.bluetoothIsOn()
            if CLLocationManager.authorizationStatus() != .authorizedAlways {
                validateLocationPermissions()
            }
            break
        case .poweredOff, .resetting, .unauthorized, .unsupported, .unknown:
            self.delegate?.bluetoothFailureOccurred()
            break
        }
    }
    
    
    // MARK: Start/Stop beacon monitoring
    
    func startBeaconMonitoring() {
        let authStatus = CLLocationManager.authorizationStatus()
        if authStatus == .authorizedAlways {
            beaconManager.delegate = self
            beaconManager.startMonitoring(for: CLBeaconRegion(
                proximityUUID: UUID(uuidString: BeaconConstants.BeaconUuid)!,
                major: CLBeaconMajorValue(BeaconConstants.BeaconMajor), minor: CLBeaconMinorValue(BeaconConstants.BeaconMinor), identifier: BeaconConstants.BeaconIdentifier))
        } else {
            delegate?.notAuthorized()
        }
    }
    
    func stopBeaconMonitoring() {
        beaconManager.stopMonitoringForAllRegions()
        beaconManager.delegate = nil
    }
    
    
    // MARK: ESTBeaconManagerDelegate
    
    func beaconManager(_ manager: Any, didEnter region: CLBeaconRegion) {
        self.delegate?.beaconFound()
    }
    
    func beaconManager(_ manager: Any, monitoringDidFailFor region: CLBeaconRegion?, withError error: Error) {
        self.delegate?.beaconNotFound()
    }
 
    func beaconManager(_ manager: Any, didFailWithError error: Error) {
        self.delegate?.beaconNotFound()
    }
    
    func beaconManager(_ manager: Any, didDetermineState state: CLRegionState, for region: CLBeaconRegion) {
        switch state {
        case .outside, .unknown:
            self.delegate?.beaconNotFound()
            break
        case .inside:
            self.delegate?.beaconFound()
            break
        }
    }
}
