//
//  FlightsDataSource.swift
//  Airport KE
//
//  Created by Karol Grulling on 18/07/2017.
//  Copyright © 2017 com.krlg. All rights reserved.
//

import UIKit

class FlightsDataSource: NSObject, UITableViewDataSource, UITableViewDelegate {
    let flightsArrivalCellIdentifier = "FlightsArrivalCell"
    let flightsDepartureCellIdentifier = "FlightsDepartureCell"
    let ExpandedRowHeight = CGFloat.init(144.0)
    var flightsType: FlightType = FlightType.Departure
    var expandedRows = Set<Int>()
  
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        var flight: Flight!
        var cellId: String!
        if flightsType == FlightType.Departure {
            flight = FlightHolder.sharedHolder.departures[indexPath.row]
            cellId = flightsDepartureCellIdentifier
        } else {
            flight = FlightHolder.sharedHolder.arrivals[indexPath.row]
            cellId = flightsArrivalCellIdentifier
        }
        
        let flightCell = tableView.dequeueReusableCell(withIdentifier: cellId, for: indexPath) as! FlightCell
    
        flightCell.initWithFlight(flight: flight, flightType: flightsType)
        flightCell.setExpanded(self.expandedRows.contains(indexPath.row), animated: false)
        flightCell.checkFlightBtn?.tag = indexPath.row
        
        var isFlightChecked = false
        if let flightNumber = flight.flightNumber {
            isFlightChecked = CoreDataManager.sharedManager.getFlight(flightNumber: flightNumber) != nil
        }
        flightCell.setFlightChecked(isFlightChecked)
        
        return flightCell
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if flightsType == FlightType.Departure {
            print("Departuresss: ", FlightHolder.sharedHolder.departures.count)
            return FlightHolder.sharedHolder.departures.count
        } else {
            return FlightHolder.sharedHolder.arrivals.count
        }
    }
    
    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        return ExpandedRowHeight
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        print(indexPath.row)
        guard let cell = tableView.cellForRow(at: indexPath) as? FlightCell
            else { return }
        switch cell.isExpanded()
        {
        case true:
            self.expandedRows.remove(indexPath.row)
        case false:
            self.expandedRows.insert(indexPath.row)
        }
        
        cell.setExpanded(!cell.isExpanded(), animated: true) 
        tableView.beginUpdates()
        tableView.endUpdates()
    }
}
