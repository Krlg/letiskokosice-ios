//
//  PhotoTaker.swift
//  AirportKE
//
//  Created by Karol Grulling on 29/07/2017.
//  Copyright © 2017 com.krlg. All rights reserved.
//

import UIKit

protocol PhotoTakerDelegate {
    func photoTaken(photo: UIImage)
}

class PhotoTaker: NSObject, UINavigationControllerDelegate, UIImagePickerControllerDelegate {
    static let sharedTaker = PhotoTaker()
    var picker: UIImagePickerController?
    var currentVC: UIViewController?
    var delegate: PhotoTakerDelegate?
    
    func showPhotoTypeSelectionInVC(vc: UIViewController) {
        currentVC = vc
        let alertController = UIAlertController.init(title: "Pridaj fotku", message: nil, preferredStyle: UIAlertController.Style.actionSheet)
        let takePhotoAction = UIAlertAction.init(title: "Odfoť sa", style: UIAlertAction.Style.default, handler: { alertAction in
            self.addPhotoPressed(type: .camera)
        })
        let choosePhotoAction = UIAlertAction.init(title: "Vyber fotku z galérie", style: UIAlertAction.Style.default, handler: { alertAction in
            self.addPhotoPressed(type: .photoLibrary)
        })
        let cancelAction = UIAlertAction.init(title: "Zrušiť", style: UIAlertAction.Style.cancel, handler: nil)
        alertController.addAction(takePhotoAction)
        alertController.addAction(choosePhotoAction)
        alertController.addAction(cancelAction)
        currentVC?.present(alertController, animated: true, completion: nil)
    }
    
    func addPhotoPressed(type: UIImagePickerController.SourceType) {
        picker = UIImagePickerController()
        picker?.sourceType = type
        self.picker?.delegate = self
        currentVC?.present(self.picker!, animated: true, completion: nil)
    }
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        if let image = info[UIImagePickerController.InfoKey.originalImage] as? UIImage {
            picker.dismiss(animated: true, completion: nil)
            self.delegate?.photoTaken(photo: Utils.resizeImage(image: image, newWidth: 700))
        }
    }
}
