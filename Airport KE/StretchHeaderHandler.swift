//
//  StretchHeaderHandler.swift
//  Alamofire
//
//  Created by Karol Grulling on 17/04/2018.
//

import UIKit


struct TableViewDimensions {
    let HeaderCut: CGFloat = 0
}

class StretchHeaderHandler: NSObject {
    
    var headerView: UIView!
    var newHeaderLayer: CAShapeLayer!
    var tableView: UITableView?
    var headerHeight: CGFloat?
    
    init(tableView: UITableView, headerHeight: CGFloat) {
        self.headerHeight = headerHeight
        self.tableView = tableView
    }
    
    func updateView(){
        tableView?.backgroundColor = UIColor.white
        headerView = tableView?.tableHeaderView
        tableView?.tableHeaderView = nil
        tableView?.rowHeight = UITableView.automaticDimension
        tableView?.addSubview(headerView)
        newHeaderLayer = CAShapeLayer()
        newHeaderLayer.fillColor = UIColor.black.cgColor
        headerView.layer.mask = newHeaderLayer
        let newHeight = headerHeight! - TableViewDimensions().HeaderCut/2
        tableView?.contentInset = UIEdgeInsets(top: newHeight, left: 0, bottom: 0, right: 0)
        tableView?.contentOffset = CGPoint(x: 0, y: -newHeight)
        setNewView()
    }
    
    func setNewView(){
        let newHeight = headerHeight! - TableViewDimensions().HeaderCut/2
        var getHeaderFrame =  CGRect(x: 0, y: -newHeight, width: (tableView?.bounds.width)!, height: headerHeight!)
        if (tableView?.contentOffset.y)! < newHeight {
            getHeaderFrame.origin.y = (tableView?.contentOffset.y)!
            getHeaderFrame.size.height = -(tableView?.contentOffset.y)! + TableViewDimensions().HeaderCut/2
        }
        headerView.frame = getHeaderFrame
        let cutDirection = UIBezierPath()
        cutDirection.move(to: CGPoint(x: 0, y: 0))
        cutDirection.addLine(to: CGPoint(x: getHeaderFrame.width, y: 0))
        cutDirection.addLine(to: CGPoint(x: getHeaderFrame.width, y: getHeaderFrame.height))
        cutDirection.addLine(to: CGPoint(x: 0, y: getHeaderFrame.height - TableViewDimensions().HeaderCut))
        newHeaderLayer.path = cutDirection.cgPath
    }
}
