//
//  CustomSegmentedControl.swift
//  Airport KE
//
//  Created by Ales Melichar on 30/05/2019.
//  Copyright © 2019 com.krlg. All rights reserved.
//

import UIKit

@IBDesignable
class CustomSegmentedControl: UIControl {
    var buttons = [UIButton]()
    var selector: UIView!
    var selectedSegmentIndex = 0
    var titles: [String] = []
    
    @IBInspectable
    var borderWidth: CGFloat = 0 {
        didSet {
            layer.borderWidth = borderWidth
        }
    }
    
    @IBInspectable
    var borderColor: UIColor = UIColor.clear {
        didSet {
            layer.borderColor = borderColor.cgColor
        }
    }
    
    
    @IBInspectable
    var commaSeparatedButtonTitles: String = "" {
        didSet {
            setButton()
            updateView()
        }
    }
    
    @IBInspectable
    var textColor: UIColor = .lightGray {
        didSet {
            setButton()
            updateView()
        }
    }
    
    @IBInspectable
    var selectorColor: UIColor = .white {
        didSet {
            setButton()
            updateView()
        }
    }
    
    @IBInspectable
    var selectorTextColor: UIColor = .lightGray{
        didSet {
            setButton()
            updateView()
        }
    }
    
    
    override func draw(_ rect: CGRect) {
        layer.cornerRadius = frame.height/2
        layer.backgroundColor = (UIColor.white).cgColor
        updateView()
    }
    
    @objc func buttonTapped(button: UIButton) {
        for (buttonIndex, btn) in buttons.enumerated() {
            btn.setTitleColor(textColor, for: .normal)
            if btn == button {
                selectedSegmentIndex = buttonIndex
                let selectorStartPosition = frame.width/CGFloat(buttons.count) * CGFloat(selectedSegmentIndex)
                UIView.animate(withDuration: 0.3, animations: {
                    self.selector.frame.origin.x = selectorStartPosition
                })
                
                btn.setTitleColor(selectorTextColor, for: .normal)
                
                if selectedSegmentIndex == 0 {
                    buttons[1].setTitle(titles[1], for: .normal)
                    buttons[0].setTitle("", for: .normal)
    
                }
                else if selectedSegmentIndex == 1 {
                    buttons[0].setTitle(titles[0], for: .normal)
                    buttons[1].setTitle("", for: .normal)
                }

            }
        }
        sendActions(for: .valueChanged)
    }
    
    func updateView(){
        buttons.removeAll()
        subviews.forEach { (view) in
            view.removeFromSuperview()
        }
        
        let buttonTitles = commaSeparatedButtonTitles.components(separatedBy: ",")
        
        setButton()
        
        let selectorWidth = self.frame.size.width / CGFloat(buttonTitles.count)
        let selectorStartPosition = frame.width/CGFloat(buttons.count) * CGFloat(selectedSegmentIndex)
        
        selector = UIView(frame: CGRect(x: selectorStartPosition, y: 0, width: selectorWidth, height: frame.height))
        selector.layer.cornerRadius = frame.height / 2
        selector.backgroundColor = selectorColor
        addSubview(selector)
        
        
        let stackView = UIStackView(arrangedSubviews: buttons)
        stackView.axis = .horizontal
        stackView.alignment = .fill
        stackView.distribution = .fillEqually
        addSubview(stackView)
        stackView.translatesAutoresizingMaskIntoConstraints = false
        stackView.topAnchor.constraint(equalTo: self.topAnchor).isActive = true
        stackView.bottomAnchor.constraint(equalTo: self.bottomAnchor).isActive = true
        stackView.leftAnchor.constraint(equalTo: self.leftAnchor).isActive = true
        stackView.rightAnchor.constraint(equalTo: self.rightAnchor).isActive = true
    }
    
    func setButton(){
        titles.removeAll()
        let buttonTitles = commaSeparatedButtonTitles.components(separatedBy: ",")
        
        for buttonTitle in buttonTitles {
            let button = UIButton(type: .system)
            button.setTitle(buttonTitle, for: .normal)
            button.setTitleColor(textColor, for: .normal)
            button.addTarget(self, action: #selector(buttonTapped(button:)), for: .touchUpInside)
            buttons.append(button)
            titles.append(buttonTitle)
            print(titles)
        }
        
        if selectedSegmentIndex == 0 {
            buttons[0].setTitle("", for: .normal)
        }
        else {
            buttons[1].setTitle("", for: .normal)
        }
        buttons[0].setTitleColor(selectorTextColor, for: .normal)
    }
}
