//
//  Reward.swift
//  Airport KE
//
//  Created by Karol Grulling on 24/04/2018.
//  Copyright © 2018 com.krlg. All rights reserved.
//

import UIKit
import CoreData

class Reward: NSObject {
    var rewardId: String?
    var name: String?
    var price: String?
    var thumbnail: String?
    
    init(json: [String: Any]) throws {
        print("I am initializing json rewards!")
        guard let jsonRewardId = json["id"] as? String else {
            throw SerializationError.missing("id")
        }
        guard let jsonName = json["name"] as? String else {
            throw SerializationError.missing("name")
        }
        guard let jsonPrice = json["price"] as? String else {
            throw SerializationError.missing("price")
        }
        guard let jsonThumbnail = json["thumbnail"] as? String else {
            throw SerializationError.missing("thumbnail")
        }
        
        self.rewardId = jsonRewardId
        self.name = jsonName
        self.price = jsonPrice
        self.thumbnail = jsonThumbnail
    }
    
    init(entity: NSManagedObject) {
        self.rewardId = entity.value(forKey: Constants.CoreDataConstants.RewardConstants.EntityKeyRewardId) as? String
        self.name = entity.value(forKey: Constants.CoreDataConstants.RewardConstants.EntityKeyRewardName) as? String
        self.price = entity.value(forKey: Constants.CoreDataConstants.RewardConstants.EntityKeyRewardPrice) as? String
        self.thumbnail = entity.value(forKey: Constants.CoreDataConstants.RewardConstants.EntityKeyThumbnail) as? String
    }
}
