//
//  MyAirportSwitchCell.swift
//  Airport KE
//
//  Created by Karol Grulling on 24/04/2018.
//  Copyright © 2018 com.krlg. All rights reserved.
//

import UIKit

class MyAirportSwitchCell: UITableViewCell {
    
   // @IBOutlet weak var flightsSwitch: DGRunkeeperSwitch?
    let switchBgColor = UIColor(red: 255/255.0, green: 174.0/255.0, blue: 18.0/255.0, alpha: 1.0)
    let selectedTitleColor = UIColor(red: 239.0/255.0, green: 95.0/255.0, blue: 49.0/255.0, alpha: 1.0)
    override func awakeFromNib() {
        super.awakeFromNib()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
//    func initWithSwitch() {
//        flightsSwitch?.titles = ["Arrivals", "Departures"]
//        flightsSwitch?.backgroundColor = switchBgColor
//        flightsSwitch?.selectedBackgroundColor = .white
//        flightsSwitch?.titleColor = .white
//        flightsSwitch?.selectedTitleColor = selectedTitleColor
//        flightsSwitch?.titleFont = UIFont(name: "HelveticaNeue-Medium", size: 13.0)
//        flightsSwitch?.autoresizingMask = [.flexibleWidth]
//    }
}
