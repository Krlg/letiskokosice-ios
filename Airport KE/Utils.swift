//
//  Utils.swift
//  Airport KE
//
//  Created by Karol Grulling on 04/07/2017.
//  Copyright © 2017 com.krlg. All rights reserved.
//

import UIKit

class Utils: NSObject {
    public static func dateFromString(string: String) -> Date? {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
        let date = dateFormatter.date(from: string)
        return date
     }
    
    public static func dateString(date: Date) -> String {
        let dateFormatter = DateFormatter()
        let calendar = Calendar.current
        let month = calendar.component(.month, from: date)
        let year = calendar.component(.year, from: date)
        let day = calendar.component(.day, from: date)
        let monthShort = dateFormatter.shortMonthSymbols[month-1]
        return monthShort + " " + String.init(describing: day) + ", " + String.init(describing: year)
    }
    
    public static func timeString(date: Date) -> String {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "HH:mm"
        return dateFormatter.string(from:date)
    }
    
    public static func resizeImage(image: UIImage, newWidth: CGFloat) -> UIImage {
        let scale = newWidth / image.size.width
        let newHeight = image.size.height * scale
        UIGraphicsBeginImageContext(CGSize.init(width: newWidth, height: newHeight))
        image.draw(in: CGRect.init(x: 0, y: 0, width: newWidth, height: newHeight))
        let newImage = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        return newImage!
    }
    
    public static func loadJsonFromFile(fileName: String) -> [String:Any]? {
        if let path = Bundle.main.path(forResource: fileName, ofType: "json") {
            do {
                let data = try Data(contentsOf: URL(fileURLWithPath: path), options: .mappedIfSafe)
                let jsonResult = try JSONSerialization.jsonObject(with: data, options: .mutableLeaves)
                return jsonResult as? [String:Any]
            } catch {
                print("Couldn't load JSON from file \(fileName)")
            }
        }
        return nil
    }
}
