//
//  MyAirportDataSource.swift
//  Airport KE
//
//  Created by Karol Grulling on 24/04/2018.
//  Copyright © 2018 com.krlg. All rights reserved.
//

import UIKit

class MyAirportDataSource: NSObject, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    
    let MyAirportActionCellIdentifier = "MyAirportActionCell"
    let myAirportChangeCellIdentifier = "MyAirportChangeCell"
    let MyAirportHeaderIdentifier = "MyAirportHeader"
    let MyAirportHeaderHeight = 414
//    let myAirportActions = [MyAirportAction(name: "Odfoť sa na letisku", icon: #imageLiteral(resourceName: "icon-photo"), actionType: MyAirportActionType.TAKE_PHOTO), MyAirportAction(name: "Nakúp v bufete", icon: #imageLiteral(resourceName: "icon-shopping-cart"), actionType: MyAirportActionType.SHOP), MyAirportAction(name: "Otestuj sa v kvíze", icon: #imageLiteral(resourceName: "icon-quiz"), actionType: MyAirportActionType.QUIZ)]
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        var reward: Reward!
        reward = RewardHolder.sharedHolder.rewards[indexPath.row]
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: myAirportChangeCellIdentifier, for: indexPath) as! MyAirportChangeCell
        cell.initCell(reward: reward)
        return cell
//        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: MyAirportActionCellIdentifier, for: indexPath) as! MyAirportCell
//        cell.initWithMyAirportAction(myAirportAction: myAirportActions[indexPath.row])
//        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
            print(RewardHolder.sharedHolder.rewards.count, "REWARDSSSSSSS")
        return RewardHolder.sharedHolder.rewards.count
    //        return myAirportActions.count
        }
    
    
    func collectionView(_ collectionView: UICollectionView, viewForSupplementaryElementOfKind kind: String, at indexPath: IndexPath) -> UICollectionReusableView {
        let headerView = collectionView.dequeueReusableSupplementaryView(ofKind: UICollectionView.elementKindSectionHeader, withReuseIdentifier: MyAirportHeaderIdentifier, for: indexPath) as! MyAirportHeader
        headerView.scoreLabel?.text = String(ScoreManager.sharedManager.getStoredScore())
        return headerView
    }
    
    func collectionView(_ collectionView: UICollectionView,
                        layout collectionViewLayout: UICollectionViewLayout,
                        referenceSizeForHeaderInSection section: Int) -> CGSize {
        return CGSize.init(width: Int(collectionView.frame.width), height: MyAirportHeaderHeight)
    }
}
