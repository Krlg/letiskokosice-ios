//
//  MyAirportVC.swift
//  Airport KE
//
//  Created by Karol Grulling on 24/04/2018.
//  Copyright © 2018 com.krlg. All rights reserved.
//

import UIKit
import BetterSegmentedControl
//import DGRunkeeperSwitch

enum BeaconState {
    case BLUETOOTH_OFF
    case BLUETOOTH_ON
    case NOT_AUTHORIZED
    case OUT_OF_RANGE
    case LOCALIZED
}

enum MyAirportActionType {
    case TAKE_PHOTO
    case SHOP
    case QUIZ
}

struct MyAirportAction {
    var name: String
    var icon: UIImage
    var actionType: MyAirportActionType
}

struct MyAirportRewards {
    var name: String
    var thumbnail: UIImage
    var price: String
}

class MyAirportVC: UIViewController, UITableViewDelegate,  UICollectionViewDelegate, BeaconManagerDelegate, PhotoTakerDelegate, QuizManagerDelegate {
    
    @IBOutlet weak var myAirportCollection: UICollectionView?
    @IBOutlet weak var beaconLocalizationView: UIView?
    @IBOutlet weak var turnBluetoothOnView: UIView?
    @IBOutlet weak var checkInView: UIView?
    @IBOutlet weak var notInAirportView: UIView?
    @IBOutlet weak var takePhotoResultView: UIView?
    @IBOutlet weak var takePhotoSuccessView: UIView?
    @IBOutlet weak var takePhotoFailureView: UIView?
    @IBOutlet weak var takenPhotoImageView: UIImageView?
    @IBOutlet weak var takenPhotoContainerView: UIView?
    @IBOutlet weak var quizContainerView: UIView?
    @IBOutlet weak var questionsContainerView: UIView?
    @IBOutlet weak var checkInBtn: UIButton!
    @IBOutlet weak var checkInLoader: UIActivityIndicatorView!
    @IBOutlet weak var backButton: UIBarButtonItem!
    @IBOutlet weak var switchControl: BetterSegmentedControl!
    
    let CellHeight: CGFloat = 49
    let HeaderHeight: CGFloat = 233
    let dataSource = MyAirportDataSource()
    var currentBeaconState = BeaconState.OUT_OF_RANGE
    
    var backButtonEnabled: Bool = false
    
    private let beaconFoundAtStoreKey = "beaconFoundAtStoreKey"
    private var _beaconFoundAt: Date!
    var beaconFoundAt: Date? {
        get {
            if _beaconFoundAt == nil {
                let interval = UserDefaults.standard.double(forKey: beaconFoundAtStoreKey)
                if interval > 0 {
                    _beaconFoundAt = Date(timeIntervalSince1970: interval)
                }
            }
            return _beaconFoundAt
        }
        set {
            if let date = newValue {
                UserDefaults.standard.set(date.timeIntervalSince1970, forKey: beaconFoundAtStoreKey)
            } else {
                UserDefaults.standard.removeObject(forKey: beaconFoundAtStoreKey)
            }
            UserDefaults.standard.synchronize()
            _beaconFoundAt = newValue
        }
    }
    let beaconFoundValidity: Double = 60 * 60 * 2 // 2 hours
    
    override func viewDidLoad() {
        super.viewDidLoad()
        turnBluetoothOnView?.superview?.layer.cornerRadius = 8
        myAirportCollection?.dataSource = dataSource
        myAirportCollection?.delegate = self
        PhotoTaker.sharedTaker.delegate = self
        backButtonEnabled = false
        checkBackButton()
        handleBeaconView()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        if let beaconFoundAt = beaconFoundAt, Date().timeIntervalSince1970 - beaconFoundAt.timeIntervalSince1970 < beaconFoundValidity {
            currentBeaconState = BeaconState.LOCALIZED
            handleBeaconView()
        } else {
            resetBeaconViewIfNeeded()
        }
    }
    
    func photoTaken(photo: UIImage) {
        takenPhotoImageView?.image = photo
        AnimationUtils.fadeInView(view: takenPhotoContainerView!)
    }
    
    @IBAction func takePhotoAgain() {
        PhotoTaker.sharedTaker.showPhotoTypeSelectionInVC(vc: self)
    }
    
    @IBAction func dismissPhotoResult() {
        AnimationUtils.fadeOutView(view: takePhotoResultView!)
    }
    
    @IBAction func sharePhotoPressed() {
        let imageToShare = [takenPhotoImageView?.image]
        let activityViewController = UIActivityViewController(activityItems: imageToShare, applicationActivities: nil)
        activityViewController.popoverPresentationController?.sourceView = self.view
        activityViewController.completionWithItemsHandler = { (activity, success, items, error) in
            AnimationUtils.fadeOutView(view: self.takenPhotoContainerView!)
            self.showPhotoSharingViewForResult(success: success)
            if (success) {
                ScoreManager.sharedManager.increaseScore(scoreToAdd: ScoreManager.PhotoBonus)
                self.myAirportCollection?.reloadData()
            }
        }
        self.present(activityViewController, animated: true, completion: nil)
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        print(indexPath.row)
//        let selectedAction = dataSource.rewards[indexPath.row]
        guard let cell = collectionView.cellForItem(at: indexPath) as? MyAirportChangeCell
            else { return }
//        let selectedAction = dataSource.myAirportActions[indexPath.row]
//        switch selectedAction.actionType {
//        case .TAKE_PHOTO:
//            PhotoTaker.sharedTaker.showPhotoTypeSelectionInVC(vc: self)
//            break
//        case .QUIZ:
//            self.navigationItem.setHidesBackButton(false, animated: true)
//            QuizManager.sharedManager.delegate = self
//            QuizManager.sharedManager.setupWithQuiz(quiz: CoreDataManager.sharedManager.getStoredQuizes()[0], quizContainer: quizContainerView!)
//            AnimationUtils.fadeInView(view: quizContainerView!)
//            backButtonEnabled = true
//            checkBackButton()
//            break
//        default:
//            break
//        }
    }
    
    func quizCompleted() {
        AnimationUtils.fadeOutView(view: quizContainerView!)
        ScoreManager.sharedManager.increaseScore(scoreToAdd: ScoreManager.QuizBonus)
        myAirportCollection?.reloadData()
        backButtonEnabled = false
        checkBackButton()
    }
    
    @IBAction func checkIn() {
        checkInBtn.isHidden = true
        checkInLoader.startAnimating()
        BeaconManager.sharedManager.startBeaconMonitoring()
    }
    
    func showPhotoSharingViewForResult(success: Bool) {
        if (success) {
            AnimationUtils.fadeInAndOutView(view: takePhotoResultView!)
            takePhotoSuccessView?.isHidden = false
            takePhotoFailureView?.isHidden = true
        } else {
            AnimationUtils.fadeInView(view: takePhotoResultView!)
            takePhotoFailureView?.isHidden = false
            takePhotoSuccessView?.isHidden = true
        }
    }
    
    func handleBeaconView() {
        switch currentBeaconState {
        case .BLUETOOTH_OFF:
            turnBluetoothOnView?.isHidden = false
            checkInView?.isHidden = true
            notInAirportView?.isHidden = true
        case .BLUETOOTH_ON:
            turnBluetoothOnView?.isHidden = true
            checkInView?.isHidden = false
            notInAirportView?.isHidden = true
        case .NOT_AUTHORIZED:
            presentNotAuthorizedAlert()
        case .OUT_OF_RANGE:
            turnBluetoothOnView?.isHidden = true
            checkInView?.isHidden = true
            notInAirportView?.isHidden = false
        case .LOCALIZED:
            beaconLocalizationView?.isHidden = true
        }
        checkInBtn.isHidden = false
        checkInLoader.stopAnimating()
    }
    
    func resetBeaconViewIfNeeded() {
        if currentBeaconState != .LOCALIZED {
            BeaconManager.sharedManager.setupWithDelegate(delegate: self)
        }
    }
    
    func beaconFound() {
        currentBeaconState = BeaconState.LOCALIZED
        handleBeaconView()
        BeaconManager.sharedManager.stopBeaconMonitoring()
        ScoreManager.sharedManager.increaseScore(scoreToAdd: ScoreManager.CheckInBonus)
        myAirportCollection?.reloadData()
        beaconFoundAt = Date()
    }
    
    func beaconNotFound() {
        currentBeaconState = BeaconState.OUT_OF_RANGE
        handleBeaconView()
    }
    
    func bluetoothIsOn() {
        currentBeaconState = BeaconState.BLUETOOTH_ON
        handleBeaconView()
    }
    
    func bluetoothFailureOccurred() {
        currentBeaconState = BeaconState.BLUETOOTH_OFF
        handleBeaconView()
    }
    
    func notAuthorized() {
        currentBeaconState = BeaconState.NOT_AUTHORIZED
        handleBeaconView()
    }
    
    func checkBackButton(){
        if backButtonEnabled == false {
            self.backButton.isEnabled = false
            self.backButton.tintColor = .clear
            self.navigationController?.popViewController(animated: true)
        }
        else {
            self.backButton.isEnabled = true
            self.backButton.tintColor = UIColor(red: 246/255.0, green: 188/255.0, blue: 70/255.0, alpha: 1)
        }
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    @IBAction func backButtonInit(_ sender: Any) {
        AnimationUtils.fadeOutView(view: quizContainerView!)
        myAirportCollection?.reloadData()
        backButtonEnabled = false
        checkBackButton()
    }
    
    private func presentNotAuthorizedAlert() {
        let alert = UIAlertController(title: "Chyba", message: "Povoľte určovanie polohy v nastaveniach.", preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "Zrušiť", style: .cancel, handler: nil))
        alert.addAction(UIAlertAction(title: "Nastavenia", style: .default) { action in
            guard let settingsUrl = URL(string: UIApplication.openSettingsURLString) else {
                return
            }
            if UIApplication.shared.canOpenURL(settingsUrl) {
                UIApplication.shared.open(settingsUrl, completionHandler: nil)
            }
        })
        present(alert, animated: true)
    }

//    @IBAction func switchControlChange(_ sender: BetterSegmentedControl) {
//        if sender.index == 0 {
////            myAirportChangeCell.isHidden = true;
//         }
//        else {
//            myAirportChangeCell.isHidden = false;
//
//        }
//    }
}
