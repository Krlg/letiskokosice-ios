//
//  MyFlightsVC.swift
//  Airport KE
//
//  Created by Karol Grulling on 17/04/2018.
//  Copyright © 2018 com.krlg. All rights reserved.
//

import UIKit

class MyFlightsVC: UIViewController, UIScrollViewDelegate, UITableViewDelegate, FlightRemovedDelegate  {
    
    @IBOutlet weak var flightsTable: UITableView?
    @IBOutlet weak var emptyView: UIView?
    let CellHeight: CGFloat = 273
    let HeaderHeight: CGFloat = 227
    @IBOutlet weak var selectFlightBtn: UIButton!
    
    let dataSource = MyFlightsDataSource()
    var stretchHeaderHandler: StretchHeaderHandler?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        flightsTable?.dataSource = dataSource
        dataSource.flightRemovedDelegate = self
        flightsTable?.delegate = self
        emptyView?.superview?.bringSubviewToFront(emptyView!)
        stretchHeaderHandler = StretchHeaderHandler.init(tableView: flightsTable!, headerHeight: HeaderHeight)
        stretchHeaderHandler?.updateView()
        
        selectFlightBtn?.layer.cornerRadius = selectFlightBtn!.bounds.height / 2
        selectFlightBtn.imageView?.contentMode = .scaleAspectFit
     }
    
    override func viewWillAppear(_ animated: Bool) {
        handleFlights()
        flightsTable?.reloadData()
    }
    
    func handleFlights() {
        dataSource.flights = CoreDataManager.sharedManager.getStoredFlights()
        if dataSource.flights.count == 0 {
            emptyView?.isHidden = false
        } else {
            emptyView?.isHidden = true
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        stretchHeaderHandler?.setNewView()
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return CellHeight
    }
    
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return UITableView.automaticDimension
    }
 
    func flightRemoved() {
        handleFlights()
    }
    
    @IBAction func showFlights() {
        self.tabBarController?.selectedIndex = 0
    }
}
