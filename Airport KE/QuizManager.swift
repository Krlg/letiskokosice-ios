//
//  QuizManager.swift
//  Airport KE
//
//  Created by Karol Grulling on 24/07/2018.
//  Copyright © 2018 com.krlg. All rights reserved.
//

import UIKit

protocol QuizManagerDelegate {
    func quizCompleted()
}

class QuizManager: NSObject, UIPageViewControllerDelegate, UIPageViewControllerDataSource {
    
    static let sharedManager = QuizManager()
    var pageViewController: UIPageViewController?
    private var currentIndex: Int = 0
    var pendingIndex: Int?
    private var quiz: Quiz?
    var delegate: QuizManagerDelegate?
 
    func setupWithQuiz(quiz: Quiz, quizContainer: UIView) {
        self.quiz = quiz
        setupPageVC(container: quizContainer)
    }
    
    func quizCompleted() {
        self.delegate?.quizCompleted()
    }

    func getQuestionCount() -> Int {
        if let totalCount = quiz?.questions.count {
            return totalCount
        } else {
            return 0
        }
    }
    
    func getQuestionForIndex(index: Int) -> Question {
        return (quiz?.questions[index])!
    }
    
    func setupPageVC(container: UIView) {
        pageViewController = UIStoryboard.init(name: Constants.StoryboardConstants.StoryboardMain, bundle: Bundle.main).instantiateViewController(withIdentifier: Constants.StoryboardConstants.QuizPageVC) as? UIPageViewController
        pageViewController?.delegate = self
        pageViewController?.setViewControllers([getViewControllerAtIndex(index: 0) as! QuestionVC], direction: UIPageViewController.NavigationDirection.forward, animated: false, completion: nil)
        container.addSubview((pageViewController?.view)!)
        pageViewController?.view.frame = (pageViewController?.view.superview?.bounds)!
        currentIndex = 0
    }
    
    func moveToNextQuestion() {
        currentIndex+=1
        pageViewController?.setViewControllers([getViewControllerAtIndex(index: currentIndex)], direction: UIPageViewController.NavigationDirection.forward, animated: false, completion: nil)
    }
    
    func pageViewController(_ pageViewController: UIPageViewController, viewControllerBefore viewController: UIViewController) -> UIViewController? {
        let pageContent = viewController as! QuizBaseVC
        var index = pageContent.pageIndex
        if ((index == 0) || (index == NSNotFound))
        {
            return nil
        }
        index -= 1;
        return getViewControllerAtIndex(index: index)
    }
    
    func pageViewController(_ pageViewController: UIPageViewController, viewControllerAfter viewController: UIViewController) -> UIViewController? {
        let pageContent: QuizBaseVC = viewController as! QuizBaseVC
        var index = pageContent.pageIndex
        if (index == (quiz?.questions.count)!+1)
        {
            return nil;
        }
        index += 1;
        return getViewControllerAtIndex(index: index)
    }
    
    func getViewControllerAtIndex(index: NSInteger) -> UIViewController
    {
        if (index == (quiz?.questions.count)!) {
            let resultsVC = UIStoryboard.init(name: Constants.StoryboardConstants.StoryboardMain, bundle: Bundle.main).instantiateViewController(withIdentifier: Constants.StoryboardConstants.QuizResultVC) as! QuizResultVC
            return resultsVC
        } else {
            let quizVC = UIStoryboard.init(name: Constants.StoryboardConstants.StoryboardMain, bundle: Bundle.main).instantiateViewController(withIdentifier: Constants.StoryboardConstants.QuestionVC) as! QuestionVC
            quizVC.question = quiz?.questions[index]
            quizVC.pageIndex = index
            return quizVC
        }
    }
    
    func presentationCountForPageViewController(pageViewController: UIPageViewController) -> Int {
        return (quiz?.questions.count)!+1
    }
    
    func pageViewController(_ pageViewController: UIPageViewController, willTransitionTo pendingViewControllers: [UIViewController]) {
        let pendingVC = pendingViewControllers.first as! QuizBaseVC
        pendingIndex = pendingVC.pageIndex
    }
    
    func pageViewController(_ pageViewController: UIPageViewController, didFinishAnimating finished: Bool, previousViewControllers: [UIViewController], transitionCompleted completed: Bool) {
        if completed {
            currentIndex = pendingIndex!
        }
    }
}
