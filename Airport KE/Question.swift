//
//  Question.swift
//  Airport KE
//
//  Created by Karol Grulling on 24/04/2018.
//  Copyright © 2018 com.krlg. All rights reserved.
//

import UIKit
import CoreData

class Question: NSObject {
    var text: String?
    var order: Int?
    var options = [Option]()
    var answer: Option?
    
    init(json: [String: Any]) throws {
        guard let jsonText = json["text"] as? String else {
            throw SerializationError.missing("text")
        }
        guard let jsonOrder = json["order"] as? Int else {
            throw SerializationError.missing("order")
        }
        guard let jsonOptions = json["options"] as? [[String:Any]] else {
            throw SerializationError.missing("options")
        }
        
        self.text = jsonText
        self.order = jsonOrder
        do {
            for jsonOption in jsonOptions {
                options.append(try Option.init(json: jsonOption))
            }
        } catch let error as NSError {
            print("Couldn't parse options for a question \(String(describing: self.text)). Error: \(error.description)")
        }
    }
    
    init(entity: NSManagedObject) {
        self.text = entity.value(forKey: Constants.CoreDataConstants.QuizConstants.EntityKeyQuestionText) as? String
        self.order = entity.value(forKey: Constants.CoreDataConstants.QuizConstants.EntityKeyQuestionOrder) as? Int
        let options = entity.value(forKey: Constants.CoreDataConstants.QuizConstants.EntityKeyQuestionOptions) as! NSSet
        let optionsArray = options.allObjects
        for optionEntity in optionsArray {
            self.options.append(Option.init(entity: optionEntity as! NSManagedObject))
        }
    }
    
    func setAnswer(answer: Option) {
        self.answer = answer
    }
    
    func correctAnswer() -> Option {
        return (options.enumerated().first(where: {$0.element.isCorrect == true})?.element)!
    }
}
