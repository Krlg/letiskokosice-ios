//
//  QuizResultVC.swift
//  Airport KE
//
//  Created by Karol Grulling on 27/07/2018.
//  Copyright © 2018 com.krlg. All rights reserved.
//

import UIKit

class QuizResultVC: QuizBaseVC, UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    
    let QuizResultCellIdentifier = "QuizResultCell"
    let QuizResultHeaderIdentifier = "QuizResultHeader"
    let QuizResultFooterIdentifier = "QuizResultFooter"
    let QuizResultHeaderHeight = 150        

    @IBOutlet weak var quizResultCollection: UICollectionView?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        quizResultCollection?.dataSource = self
    }
    
    @IBAction func quizCompletedPressed() {
        QuizManager.sharedManager.quizCompleted()
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: QuizResultCellIdentifier, for: indexPath) as! QuizResultCell
        cell.initWithQuestion(question: QuizManager.sharedManager.getQuestionForIndex(index: indexPath.row))
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return QuizManager.sharedManager.getQuestionCount()
    }
    
    func collectionView(_ collectionView: UICollectionView, viewForSupplementaryElementOfKind kind: String, at indexPath: IndexPath) -> UICollectionReusableView {
        switch kind {
        case UICollectionView.elementKindSectionHeader:
            let headerView = collectionView.dequeueReusableSupplementaryView(ofKind: UICollectionView.elementKindSectionHeader, withReuseIdentifier: QuizResultHeaderIdentifier, for: indexPath)
            return headerView
        case UICollectionView.elementKindSectionFooter:
            let footerView = collectionView.dequeueReusableSupplementaryView(ofKind: UICollectionView.elementKindSectionFooter, withReuseIdentifier: QuizResultFooterIdentifier, for: indexPath)
            return footerView
        default:
            return UICollectionReusableView()
        }
    }
    
    func collectionView(_ collectionView: UICollectionView,
                        layout collectionViewLayout: UICollectionViewLayout,
                        referenceSizeForHeaderInSection section: Int) -> CGSize {
        return CGSize.init(width: Int(collectionView.frame.width), height: QuizResultHeaderHeight)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
}
