//
//  QuizResultCell.swift
//  Airport KE
//
//  Created by Karol Grulling on 27/07/2018.
//  Copyright © 2018 com.krlg. All rights reserved.
//

import UIKit

class QuizResultCell: UICollectionViewCell {
    
    @IBOutlet weak var resultTitle: UILabel?
    
    func initWithQuestion(question:Question) {
        if question.answer == question.correctAnswer() {
            resultTitle?.textColor = UIColor.green
            resultTitle?.text = "\(String(describing: question.order!)). Správne"
        } else {
            resultTitle?.textColor = UIColor.red
            resultTitle?.text = "\(String(describing: question.order!)). Nesprávne"
        }
    }
}
