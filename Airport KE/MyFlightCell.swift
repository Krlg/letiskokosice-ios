//
//  MyFlightCell.swift
//  Airport KE
//
//  Created by Karol Grulling on 17/04/2018.
//  Copyright © 2018 com.krlg. All rights reserved.
//

import UIKit

class MyFlightCell: UITableViewCell {
    
    @IBOutlet weak var originLabel: UILabel?
    @IBOutlet weak var destinationLabel: UILabel?
    @IBOutlet weak var originCityLabel: UILabel?
    @IBOutlet weak var destinationCityLabel: UILabel?

    @IBOutlet weak var flightNumberLabel: UILabel?
    @IBOutlet weak var flightTypeLabel: UILabel?
    @IBOutlet weak var flightTimeLabel: UILabel?
    @IBOutlet weak var flightDateLabel: UILabel?
    
    @IBOutlet weak var containerView: UIView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        applyContainerStyle()
    }
    
    func applyContainerStyle() {
        containerView.layer.cornerRadius = 10
        containerView.layer.shadowColor = UIColor.darkGray.cgColor
        containerView.layer.shadowRadius = 6
        containerView.layer.shadowOffset = CGSize(width: 6, height: 6)
        containerView.layer.shadowOpacity = 0.7
        containerView.layer.masksToBounds = false
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
    func initWithFlight(flight: Flight){
        let index = flight.city?.index((flight.city?.startIndex)!, offsetBy: 3)
        originLabel?.text = flight.city?.substring(to: index!)
        flightTimeLabel?.text = Utils.timeString(date: flight.scheduledTime!)
        flightDateLabel?.text = Utils.dateString(date: flight.scheduledTime!)
        flightNumberLabel?.text = "FLIGHT \(String(describing: flight.flightNumber!))"
        switch flight.flightType {
        case .Departure?:
            originLabel?.text = "KSC"
            originCityLabel?.text = "KOŠICE"
            destinationLabel?.text = flight.city?.substring(to: index!).uppercased()
            destinationCityLabel?.text = flight.city
            flightTypeLabel?.text = "DEPARTURE TIME"
        case .Arrival?:
            originLabel?.text = flight.city?.substring(to: index!).uppercased()
            originCityLabel?.text = flight.city
            destinationLabel?.text = "KSC"
            destinationCityLabel?.text = "KOŠICE"
            flightTypeLabel?.text = "ARRIVAL TIME"
        default:
            break
        }
    }
}
