//
//  FlightCell.swift
//  Airport KE
//
//  Created by Karol Grulling on 18/07/2017.
//  Copyright © 2017 com.krlg. All rights reserved.
//

import UIKit

class FlightCell: UITableViewCell {
    
    let FlightDetailsHeightConstant:CGFloat = 234.0
    let onTimeLabelColor = UIColor(red: 126/255.0, green: 187.0/255.0, blue: 52.0/255.0, alpha: 1.0) // 7EBB34
    let delayedLabelColor = UIColor(red: 206/255.0, green: 44.0/255.0, blue: 45.0/255.0, alpha: 1.0) // CE2C2D
    
    @IBOutlet weak var aptShortLabel: UILabel?
    @IBOutlet weak var cityLabel: UILabel?
    @IBOutlet weak var scheduledTimeLabel: UILabel?
    @IBOutlet weak var scheduledDayLabel: UILabel?
    @IBOutlet weak var flightCodeLabel: UILabel?
    @IBOutlet weak var gateShortLabel: UILabel?
    @IBOutlet weak var gateShortBottomLabel: UILabel?
    @IBOutlet weak var flightDetailsHeightConstraint: NSLayoutConstraint!
    @IBOutlet weak var flightDetailsContainer: UIView?
    @IBOutlet weak var fromTitleLabel: UILabel?
    @IBOutlet weak var flightTypeLabel: UILabel?
    @IBOutlet weak var flightDetailsTitleLabel: UILabel!
    @IBOutlet weak var flightDetailsArrivalDateLabel: UILabel!
    @IBOutlet weak var flightDetailsArrivalTimeLabel: UILabel!
    @IBOutlet weak var delayedLabel: UILabel!
    @IBOutlet weak var delayedLabelBottom: UILabel!
    
    //From
    @IBOutlet weak var originLabel: UILabel?
    @IBOutlet weak var originCityLabel: UILabel?
    
    //To
    @IBOutlet weak var destLabel: UILabel?
    @IBOutlet weak var destCityLabel: UILabel?
    
    @IBOutlet weak var checkFlightBtn: UIButton?
    @IBOutlet weak var planeImage: UIImageView!
    
    private var _isExpanded: Bool = false
    
    func setExpanded(_ expanded: Bool, animated: Bool) {
        _isExpanded = expanded
        if animated {
            if !expanded {
                self.flightDetailsContainer?.alpha = 0
                UIView.animate(withDuration: 2, animations: {
                    self.flightDetailsHeightConstraint.constant = 0.0
                })
            } else {
                UIView.animate(withDuration: 0.2, animations: {
                    self.flightDetailsHeightConstraint.constant = self.FlightDetailsHeightConstant
                    self.flightDetailsContainer?.alpha = 1
                })
            }
        } else {
            if !expanded {
                self.flightDetailsContainer?.alpha = 0
                self.flightDetailsHeightConstraint.constant = 0.0
            } else {
                self.flightDetailsHeightConstraint.constant = self.FlightDetailsHeightConstant
                self.flightDetailsContainer?.alpha = 1
            }
        }
    }
    
    func isExpanded() -> Bool {
        return _isExpanded
    }
    
    let enabledBtnColor = UIColor(red: 255/255.0, green: 174.0/255.0, blue: 18.0/255.0, alpha: 1.0)
    let disabledBtnColor = UIColor.gray
    func setFlightChecked(_ checked: Bool) {
        checkFlightBtn?.isEnabled = !checked
        checkFlightBtn?.backgroundColor = checked ? disabledBtnColor : enabledBtnColor
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        checkFlightBtn?.setTitle("CHECK YOUR FLIGHT", for: .normal)
        checkFlightBtn?.setTitle("FLIGHT CHECKED", for: .disabled)
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        checkFlightBtn?.layer.cornerRadius = checkFlightBtn!.bounds.height / 2
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
    func initWithFlight(flight: Flight, flightType: FlightType){
        let index = flight.city?.index((flight.city?.startIndex)!, offsetBy: 3)
        self.aptShortLabel?.text = flight.city?.substring(to: index!).uppercased()
        cityLabel?.text = flight.city?.uppercased()
        scheduledDayLabel?.text = Utils.dateString(date: flight.scheduledTime!).uppercased()
        scheduledTimeLabel?.text = Utils.timeString(date: flight.scheduledTime!)
        flightCodeLabel?.text = flight.flightNumber
        flightDetailsTitleLabel.text = String(format: "FLIGHT %@", flight.flightNumber ?? "DETAILS")
        flightDetailsArrivalDateLabel.text = Utils.dateString(date: flight.scheduledTime!).uppercased()
        flightDetailsArrivalTimeLabel.text = Utils.timeString(date: flight.scheduledTime!)
        if let isOnTime = flight.isOnTime() {
            delayedLabel?.isHidden = false
            delayedLabelBottom?.isHidden = false
            delayedLabel?.backgroundColor = isOnTime ? onTimeLabelColor : delayedLabelColor
            delayedLabelBottom?.backgroundColor = isOnTime ? onTimeLabelColor : delayedLabelColor
            delayedLabel?.text = isOnTime ? "ON TIME" : "DELAY"
            delayedLabelBottom?.text = isOnTime ? "ON TIME" : "DELAY"
        } else {
            delayedLabel?.isHidden = true
            delayedLabelBottom?.isHidden = true
        }
 
        switch flightType {
        case .Departure:
            fromTitleLabel?.text = "To"
            originLabel?.text = "KSC"
            originCityLabel?.text = "KOŠICE"
            destLabel?.text = flight.city?.substring(to: index!).uppercased()
            destCityLabel?.text = flight.city?.uppercased()
            flightTypeLabel?.text = "DEPARTURE TIME"
            planeImage.image = UIImage(named: "departures")?.withRenderingMode(.alwaysOriginal)
            gateShortLabel?.text = flight.gate?.count ?? 0 > 0 ? flight.gate : "---"
            gateShortBottomLabel?.text = gateShortLabel?.text
        case .Arrival:
            fromTitleLabel?.text = "From"
            originLabel?.text = flight.city?.substring(to: index!).uppercased()
            originCityLabel?.text = flight.city?.uppercased()
            destCityLabel?.text = "KOŠICE"
            destLabel?.text = "KSC"
            flightTypeLabel?.text = "ARRIVAL TIME"
            planeImage.image = UIImage(named: "arrivals")?.withRenderingMode(.alwaysOriginal)
        }
    }
}
