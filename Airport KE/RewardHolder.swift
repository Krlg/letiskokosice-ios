//
//  RewardHolder.swift
//  Airport KE
//
//  Created by Ales Melichar on 08/11/2019.
//  Copyright © 2019 com.krlg. All rights reserved.
//

import Foundation

class RewardHolder: NSObject {
    static let sharedHolder = RewardHolder()
    var rewards = [Reward]()
}
