//
//  Option.swift
//  Airport KE
//
//  Created by Karol Grulling on 24/04/2018.
//  Copyright © 2018 com.krlg. All rights reserved.
//

import UIKit
import CoreData

class Option: NSObject {
    var name: String?
    var isCorrect: Bool?
    
    init(json: [String: Any]) throws {
        guard let jsonName = json["name"] as? String else {
            throw SerializationError.missing("name")
        }
        
        guard let jsonIsCorrect = json["is_correct"] as? Bool else {
            throw SerializationError.missing("is_correct")
        }
        
        self.name = jsonName
        self.isCorrect = jsonIsCorrect
    }
    
    init(entity: NSManagedObject) {
        self.name = entity.value(forKey: Constants.CoreDataConstants.QuizConstants.EntityKeyOptionName) as? String
        self.isCorrect = entity.value(forKey: Constants.CoreDataConstants.QuizConstants.EntityKeyOptionIsCorrect) as? Bool
    }
}
