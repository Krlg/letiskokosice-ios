//
//  CoreDataManager.swift
//  Airport KE
//
//  Created by Karol Grulling on 16/04/2018.
//  Copyright © 2018 com.krlg. All rights reserved.
//

import UIKit
import CoreData
import Alamofire

class CoreDataManager: NSObject {
    
    static let fligtsDidChangeNotificationName = Notification.Name.init("fligtsDidChangeNotificationName")
    
    let context: NSManagedObjectContext?
    static let sharedManager = CoreDataManager()
    private override init() {
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        self.context = appDelegate.persistentContainer.viewContext
    }
    
    func createQuestionEntity(question: Question) -> NSManagedObject? {
        if let questionEntityDescription = NSEntityDescription.entity(forEntityName: Constants.CoreDataConstants.QuizConstants.EntityNameQuestion, in: self.context!) {
            let questionEntity = NSManagedObject(entity: questionEntityDescription, insertInto: self.context)
            questionEntity.setValue(question.text, forKey: Constants.CoreDataConstants.QuizConstants.EntityKeyQuestionText)
            questionEntity.setValue(question.order, forKey: Constants.CoreDataConstants.QuizConstants.EntityKeyQuestionOrder)
            var optionArray = [NSManagedObject]()
            for option in question.options {
                optionArray.append(createOptionEntity(option: option)!)
            }
            let optionSet = NSSet().addingObjects(from: optionArray)
            questionEntity.setValue(optionSet, forKey: Constants.CoreDataConstants.QuizConstants.EntityKeyQuestionOptions)
            return questionEntity
        }
        return nil
    }
    
    func createOptionEntity(option: Option) -> NSManagedObject? {
        if let optionEntityDescription = NSEntityDescription.entity(forEntityName: Constants.CoreDataConstants.QuizConstants.EntityNameOption, in: self.context!) {
            let optionEntity = NSManagedObject(entity: optionEntityDescription, insertInto: self.context)
            optionEntity.setValue(option.name, forKey: Constants.CoreDataConstants.QuizConstants.EntityKeyOptionName)
            optionEntity.setValue(option.isCorrect, forKey: Constants.CoreDataConstants.QuizConstants.EntityKeyOptionIsCorrect)
            return optionEntity
        }
        return nil
    }
    
    func saveQuiz(quiz: Quiz) {
        if let quizEntityDescription = NSEntityDescription.entity(forEntityName: Constants.CoreDataConstants.QuizConstants.EntityNameQuiz, in: self.context!) {
            let quizEntity = NSManagedObject(entity: quizEntityDescription, insertInto: self.context)
            quizEntity.setValue(quiz.quizId, forKey: Constants.CoreDataConstants.QuizConstants.EntityKeyQuizId)
            quizEntity.setValue(quiz.title, forKey: Constants.CoreDataConstants.QuizConstants.EntityKeyQuizTitle)
            quizEntity.setValue(quiz.reward, forKey: Constants.CoreDataConstants.QuizConstants.EntityKeyReward)
            var questionArray = [NSManagedObject]()
            for question in quiz.questions {
                questionArray.append(createQuestionEntity(question: question)!)
            }
            let questionSet = NSSet().addingObjects(from: questionArray)
            quizEntity.setValue(questionSet, forKey: Constants.CoreDataConstants.QuizConstants.EntityKeyQuestions)
            do {
                try self.context?.save()
            } catch {
                print("Couldn't store Quiz to my quizes with error: \(error)")
            }
        }
    }
    
    func saveFlight(flight:Flight) {
        if let entity = NSEntityDescription.entity(forEntityName: Constants.CoreDataConstants.FlightConstants.EntityNameMyFlight, in: self.context!) {
            let myFlight = NSManagedObject(entity: entity, insertInto: self.context)
            myFlight.setValue(flight.flightNumber, forKey: Constants.CoreDataConstants.FlightConstants.EntityKeyFlightNumber)
            myFlight.setValue(flight.city, forKey: Constants.CoreDataConstants.FlightConstants.EntityKeyCity)
            myFlight.setValue(flight.estimatedTime, forKey: Constants.CoreDataConstants.FlightConstants.EntityKeyScheduledTime)
            myFlight.setValue(flight.airline, forKey: Constants.CoreDataConstants.FlightConstants.EntityKeyAirline)
            myFlight.setValue(flight.flightType?.rawValue, forKey: Constants.CoreDataConstants.FlightConstants.EntityKeyFlightType)
        }
        do {
            try self.context?.save()
            NotificationCenter.default.post(name: CoreDataManager.fligtsDidChangeNotificationName, object: nil)
        } catch {
            print("Couldn't store Flight to my flights with error: \(error)")
        }
    }
    
    func getStoredFlights() -> [Flight] {
        var flights = [Flight]()
        let fetchRequest = NSFetchRequest<NSFetchRequestResult>(entityName: Constants.CoreDataConstants.FlightConstants.EntityNameMyFlight)
        do {
            if let myFlights = try self.context?.fetch(fetchRequest) {
                for flightEntity in myFlights {
                    flights.append(Flight.init(entity: flightEntity as! NSManagedObject))
                }
            }
        } catch {
            print("An error occurred while retrieving stored flights : \(error)")
        }
        return flights
    }
    
    func getFlight(flightNumber: String) -> Flight? {
        let fetchRequest = NSFetchRequest<NSFetchRequestResult>(entityName: Constants.CoreDataConstants.FlightConstants.EntityNameMyFlight)
        let predicate = NSPredicate(format: "flightNumber == %@", flightNumber)
        fetchRequest.predicate = predicate
        do {
            if let flight = try self.context?.fetch(fetchRequest).first {
                return Flight.init(entity: flight as! NSManagedObject)
            }
        } catch {
            print("An error occurred while retrieving stored flights : \(error)")
        }
        return nil
    }
    
    func saveRewards(reward: Reward) {
        if let entity = NSEntityDescription.entity(forEntityName: Constants.CoreDataConstants.RewardConstants.EntityNameReward, in: self.context!) {
            let myReward = NSManagedObject(entity: entity, insertInto: self.context)
            myReward.setValue(reward.name, forKey: Constants.CoreDataConstants.RewardConstants.EntityKeyRewardName)
            myReward.setValue(reward.price, forKey: Constants.CoreDataConstants.RewardConstants.EntityKeyRewardPrice)
            myReward.setValue(reward.thumbnail, forKey: Constants.CoreDataConstants.RewardConstants.EntityKeyThumbnail)
        }
        do {
            try self.context?.save()
        } catch {
            print("Couldn't store rewards with error: \(error)")
        }
    }
    func getReward(rewardName: String) -> Reward? {
        let fetchRequest = NSFetchRequest<NSFetchRequestResult>(entityName: Constants.CoreDataConstants.RewardConstants.EntityNameReward)
        let predicate = NSPredicate(format: "name", rewardName)
        fetchRequest.predicate = predicate
        do {
            if let reward = try self.context?.fetch(fetchRequest).first {
                print("EVERYTHING IS GOOD!")
                return Reward.init(entity: reward as! NSManagedObject)
            }
        } catch {
            print("An error occurred while retrieving stored rewards : \(error)")
        }
        return nil    }
    
    func deleteStoredFlight(flightNumber: String) {
        let fetchRequest = NSFetchRequest<NSFetchRequestResult>(entityName: Constants.CoreDataConstants.FlightConstants.EntityNameMyFlight)
        let predicate = NSPredicate(format: "flightNumber == %@", flightNumber)
        fetchRequest.predicate = predicate
        do {
            if let myFlights = try self.context?.fetch(fetchRequest) {
                for flightEntity in myFlights {
                    context?.delete(flightEntity as! NSManagedObject)
                }
            }
            try context?.save()
            NotificationCenter.default.post(name: CoreDataManager.fligtsDidChangeNotificationName, object: nil)
        } catch {
            print("An error occurred while retrieving stored flights : \(error)")
        }
    }
    
    func getStoredQuizes() -> [Quiz] {
        var quizes = [Quiz]()
        let fetchRequest = NSFetchRequest<NSFetchRequestResult>(entityName: Constants.CoreDataConstants.QuizConstants.EntityNameQuiz)
        do {
            if let myQuizes = try self.context?.fetch(fetchRequest) {
                for quizEntity in myQuizes {
                    quizes.append(Quiz.init(entity: quizEntity as! NSManagedObject))
                }
            }
        } catch {
            print("An error occurred while retrieving stored quizes : \(error)")
        }
        return quizes
    }
}
