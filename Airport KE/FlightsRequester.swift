//
//  CoreRequest.swift
//  Airport KE
//
//  Created by Karol Grulling on 18/05/2017.
//  Copyright © 2017 com.krlg. All rights reserved.
//

import UIKit
import Alamofire

class FlightsRequester: CoreRequester {
    static let sharedRequester = FlightsRequester()
    
    func getFlights(completionHandler: @escaping (_ success: Bool, _ arrivals: [Flight]?, _ departures: [Flight]?) -> Void) {
        
        AF.request(Constants.URLConstants.BaseUrl + Constants.URLConstants.FlightsUrl, method: .get, parameters: nil, encoding: URLEncoding.default, headers: nil).responseJSON(completionHandler: { response in
            self.logResponse(response: response)
            print("Response String: \(response.result)")

            
            guard response.response?.statusCode == 200 else {
                completionHandler(false, nil, nil)
                return
            }
            
            switch response.result {
            case let .success(value):
                var arrivals = [Flight]()
                var departures = [Flight]()
                do {
                    let flightsResult = value as! [String:Any]
                    let arrivalsData = flightsResult["arrivals"] as! [[String:Any]]
                    print("FlightData: ", arrivalsData)

                    for flightJson in arrivalsData {
                        let flight = try Flight.init(json: flightJson, flightType: FlightType.Arrival)
                        arrivals.append(flight)
                    }
                    let departuresData = flightsResult["departures"] as! [[String:Any]]
                    
                    for flightJson in departuresData {
                        let flight = try Flight.init(json: flightJson, flightType: FlightType.Departure)
                        departures.append(flight)
                    }
                } catch let error as NSError {
                    print("Lety - Vyskytol sa error! ", error.description)
                }
                completionHandler(true, arrivals, departures)
                
            case .failure(_):
                completionHandler(false, nil, nil)
            }
        })
    }
}
